---
title: "Encrypted Email: Why and How"
author: Jackson Lee
date: 2024-06-07
description: Do you prefer to send postcards where everyone can see the message, or letters inside a sealed envelope? Email is, to the surprise of many, more akin to the former than the latter. Let us talk about how to fix this.
categories: "articles"
tags: ["tech", "encryption", "informative"]
draft: false
---

# [Encrypted Email: Why bother?](/articles)
Email is incredibly useful. No longer do we rely on the pony express or any modern, motorized equivalent. It truly is a game-changer, providing incredibly quick communications between individuals irrespective of the distance between. Unfortunately, it was not designed with privacy or security in mind. Email was developed when the internet was in its infancy, and it shows. Originally, email was so insecure that anyone who was able to watch the network traffic at any point in the transmission chain could read the contents and metadata indiscriminately - hell, even the passwords were sent unencrypted. It truly was the wild west.

## Secure email - solutions and remaining issues
While it is still technically possible to use an email server in this way, it is safe to generalize and say that nobody actually does. Instead, as the technology matured, there were two proposed solutions:

- TLS encryption
- GPG encryption

### TLS Encryption - Armored Car Approach
The first I will discuss is the "armored car" approach. This is when you encrypt the network traffic you use to send the email (think `https` instead of `http`). This is akin to sending a postcard using an armored car. It is completely secure while in transit, but once it hits the post office (meaning the mail server: Gmail, iCloud, yahoo, etc.) it is completely vulnerable. Now, maybe Google or Apple store their emails encrypted on their respective mail servers, but there really is no way of knowing just how secure these emails are while they are at rest. Additionally, any subsequent transmissions must also be encrypted or the entire thing falls apart. It is likely that Google encrypts the exchanges between their mail servers and Apple's, but do you know for certain if that is the case? If we are to trust [Google themselves](https://transparencyreport.google.com/safer-email/overview?hl=en_GB), then between Mar 3, 2024 and Jun 8, 2024, 97% of outbound emails were encrypted - I think it is safe to assume you are included in that number (assuming you are sending emails to "normal" email addresses). Again, this is predicated on trusting Google to be providing the correct information. Perhaps you do, perhaps you do not.

While it was somewhat uncommon "back in the day", nowadays TLS encryption (the aforementioned armored car approach) is practically ubiquitous. That being said, we still have to wonder about the security of the data at rest. Are your emails securely encrypted while on your computer? Possibly not, depending on the email client you use, it is possible that your emails are all downloaded and stored locally on your computer completely unencrypted. Also, as mentioned previously, we are relying on our email providers to have secure encryption enabled on their email servers to encrypt the messages stored therein. With the rate of large-scale data breaches in recent years, I am not too keen on providing this level of trust to a company.

### GPG Encryption - Secret Language Approach
Instead of securing the transmission but leaving the resting state of a message vulnerable, the "Secret Language approach" encrypts the message, turning it into a completely garbled mess illegible to anyone who does not know the "secret language" - i.e. the intended recipient and the sender themselves. This is the topic of [my GPG guide](/projects/gpg-ultimate-beginners-guide) and the solution that I prefer. This method takes the burden of trust (and responsibility) away from the email provider and places it in the hands of the user. While it has failed to catch on in popularity due to the perceived difficulty of use, if one practices sound and safe computing habits it is vastly more secure than the alternative of relying on TLS encryption alone. Additionally, it can be used in conjunction with the latter with no further action required.

## Conclusion

At the end of the day, most people will never question how their email is secured, they will likely just assume it is so and carry on. For those willing to take the extra time and effort, however, it is easy to set up GPG encryption in such a way so as to guarantee that the only one capable of reading a given email, even if your email login is compromised, remains the intended recipient and them alone.

> Security is a journey, not a destination.
