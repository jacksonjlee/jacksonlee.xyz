---
title: "Why a Blog"
date: 2024-05-12
description: Why on earth would someone start a blog in 2024? Well, let me explain!
categories: "articles"
tags: [personal]
draft: false
---

# [Why a Blog](/articles)
Writing this in 2024, one could make a solid argument that I am roughly two decades late to the blogging scene. This begs the obvious question of why I started this blog, and why now. I am not particularly fond of social media, which means that I do not really have much of an online presence. I do, however, have opinions and fleeting thoughts that I would appreciate sharing - assuming anyone actually cares to hear them, but that is a different point entirely!

As such, this blog is intended to be my digital project board where I will post thoughts on my various outlooks on life or projects I am undertaking. I could have used a more established social media platform, but for reasons that will become obvious in future posts, I am rather against their use. Additionally, there are a plethora of benefits to a blog that are either less readily available or completely absent from other platforms.

## Benefits of a Blog

Overall, I believe the greatest benefit of starting one's own blog is that it is completely focused on the generation of 'content'. At no point while creating an article for this blog, do I become annoyed by a character limit, restrictive image formatting constraints, or distracted by flashing posts made by others - that remains entirely within the realm of standard social media. As someone who never really took to social media, I am a very big proponent for longer form content that more completely encompasses a given idea. If I am to post a guide on how to set up a media server, or post a written article regarding my outlook on a given topic, I very much prefer to do so on a platform that does not limit me in my ability to write a long guide or article and not be forced to fit into a given word count or hesitate to post an opinioned piece for fear moderation if my thoughts are considered to be contrarian to a particular narrative.

## Downsides to a Blog

Of course, not everything is to the benefit of a prospective blogger. For starters, my ideological abstinence from established blogging websites is nothing if not a hindrance to the growth of my prospective blog. Additionally, my (also ideological) restraint from implementing advertisements or trackers on this site will result in a reduced revenue stream (read, non-existent) when compared to the potential income of "influencers".

As someone who already has a career that is not dependent on the fickle preference of the internet, I would argue that the latter point is somewhat of a moot point.

## Closing Thoughts

Overall, much like my social media profiles I probably will not be posting on here too often, but I thought this would be a fun learning experience. I have already learned a lot in the process of setting up the hosting for this, writing the various templates for hugo to generate the site, and even a bit of CI/CD in GitLab for automatically deploying the site. I had fun, so maybe some others will be interested enough to join me for the ride.
