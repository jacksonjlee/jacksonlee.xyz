---
title: Welcome
author: Jackson Lee
draft: false
---

  This is just a small personal site wherein I ramble about what's on my mind, the various projects I'm working on or have completed, and basically just yell into the digital clouds that make up our world.
