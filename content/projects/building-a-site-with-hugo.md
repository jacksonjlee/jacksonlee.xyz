---
title: Building a Site With Hugo
author: Jackson Lee
date: 2024-06-06
categories[]: projects
description: How I built this site with HUGO, why HUGO is useful, and how to use it to create your own templates.
tags: ["hugo", "webdev", "tech"]
draft: false
---

# [Building a Site with HUGO](/projects)

Going into this project, I had a basic familiarity with HTML. I did not know much about it beyond basic tags and such, but in the process of generating the template for this site I learned much about it through experimentation (my favorite way of learning!). I enjoy tinkering with something until it works just as much as I enjoy learning new things, and in my experience the best way to do the latter is to practice the former, and thus this site was born.

## Why Hugo

I knew going in that I didn't want to create a bloated mess, I enjoy simplistic web designs that are sleek and fast. I also did not want to spend weeks writing an entirely static website by hand - manually adding links and updating list pages sounded like a horrible task to relegate my future free time to doing. Some research lead me to believe that a static site, an omission of unnecessary JavaScript, and reactive CSS would get me what I desired. Thus began my journey with [HUGO](https://gohugo.io).I wasn't sure how HUGO's syntax worked, but the promise of a fast, easy-to-use, and lightweight method to building a static site provided me with the tools necessary to do what I wanted.

## How it works

Upon the installation of HUGO and the initialization of a HUGO site as outlined clearly on their online documentation [here](https://gohugo.io/getting-started/quick-start/), you are provided with a simple directory structure. We are only interested in the following:

   - Archetypes: You can think of archetypes as HUGO's term for templates, and in this directory you can find examples as well as create your own templates for whatever kind of pages you plan to create in whichever supported format you like. I chose markdown, but you can also use Emacs' ORG mode.
   - Content: the content directory is where you will place the Markdown files you plan to publish, as an example, this page was written in markdown (source can be found on [my GitLab](https://gitlab.com/jacksonjlee/jacksonlee.xyz) and placed inside of `/content/projects/`. Notice the `/projects/` subdirectory? That's because you can expand upon the directory structure provided by HUGO with "sections" inside of your `/content/` directory. I opted to use two such categories: `projects` and `articles` - but you can choose whatever names you would like for any categories you opt to utilize. For more information check out the [HUGO online documentation](https://gohugo.io/content-management/sections/).
   - Public - This directory is where HUGO will place the output HTML files. If it ends up in this folder after site generation, it's ready to be served.
   - Static - This stores any and all static content (wow, I'm so helpful), such as images, CSS, JavaScript, etc. These files will be copied to the new site without any changes.
   - Themes - Inside this directory you can place themes to use, which will bring with them their own archetypes, static content (especially CSS files!), layouts, etc. You can view the theme used to create this site on [my GitLab](https://gitlab.com/jacksonjlee/simply-darkness). To import a theme as a git submodule for your site (this is what I did for the creation of this site and the method I recommend), you can simply follow the quickstart guide or the theme's README file, replacing the respective theme setting in the config.toml file with the correct theme name.


## Why care about partials

While building out a website in HTML, you will inevitably repeat many segments of your site design over multiple pages. For example, think of Wikipedia: every page you go to will have the same overall layout of Header across the top, sidebar on the left, and footer underneath the body of the Wikipedia page in question. Some pages are lists of other pages, and follow a separate but still standardized layout. The same applies to this website; I have a header that I'd like to display on every page that provides quick links to the main page, the articles and projects pages, and an about me page, as well as a footer to provide links to [my GitLab](https://gitlab.com/jacksonjlee) and an RSS feed. If I were to manually create each page I would have to either copy these sections onto each page or rewrite them every time I wanted to implement them in a given page. HUGO does the former for me.

### Building the Partials

Partials in HUGO represent sections of HTML code that you can quickly and easily repeat across a given page in the site. As previously mentioned, I use partial files to control the header and footer for this blog. I also use a partial file to manage the `<head>` section of each page, which is the part of the HTML document that sets the metadata for the page such as the encoding, stylesheet, favicon (the little image on the tabs), etc. Partials allow you to code the HTML once and then tell HUGO to automatically import that HTML to a given page. For example, my head.html partial looks like this:

```html
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ if not .IsHome }}{{ .Title | title }} | {{ end }}{{ .Site.Title }}</title>
	<link rel="canonical" href="{{ .Site.BaseURL }}">
	<link rel='alternate' type='application/rss+xml' title="{{ .Site.Title }} RSS" href='/index.xml'>
	<link rel='stylesheet' type='text/css' href='/style.css'>
	{{- with .Site.Params.favicon }}<link rel="icon" href="{{ . }}">{{ end }}
	<meta name="description" content="{{ with .Params.description }}{{ . }}{{ else }}{{ .Summary }}{{ end }}">
	{{- if isset .Params "tags" }}<meta name="keywords" content="{{- with .Params.Tags }}{{ delimit . ", " }}{{ end }}">{{ end }}
	<meta name="robots" content="index, follow">
</head>
```

To better understand this, we can break it down into sections. The first line necessary in any HTML document is the `DOCTYPE`, which identifies it as an HTML document and specifies the HTML version to use (in our case the newest, since a specific version number is not specified). After that we set the language and a few other minutia until you get to line six. There you can see your first bit of HUGO syntax, wherein I check to see if the page is _not_ .IsHome, meaning the landing page. If the page is _not_ the landing page, which will be the case more often than not, I set the tab title to be the name of the current page, display a pipe character, then the name of the website. For this page, as an example, that comes out to:

`Building a Site with Hugo | JacksonLee.xyz`

If, on the other hand, we are on the main landing page, it simply sets the page's title to the site's title, in our case:

`JacksonLee.xyz`

### Importing the partials

From there, you should be able to figure out how the rest of the head partial does what it does. But enough about the partial -- how do we use it? After all, the partials themselves are useless until you call on them! Let's continue to examine the landing page, but this time the landing page's code itself, rather than just the partials it consists of. If you recall from the How it works section, HUGO keeps layout files inside of either `/layouts` or `/themes/theme-name/layouts/`. In our case, the layout for our landing page is located at `/themes/simply-darkness/layouts/index.html` (Note you can find the source for this theme at my [GitLab](https://www.gitlab.com/jacksonjlee/simply-darkness/). This `index.html` file consists of the following:

```html
{{- partial "head" . }}
	{{- partial "header" . }}
		{{ .Content }}
		<ul>{{ range .Sections.ByTitle }}
			<li class="section-header">
				<h2 class="section__title"><a href="{{ .RelPermalink }}">{{ .Title }}</a></h2>
				{{ with .Params.Description }}<span class="description">{{ . }}</span>{{ end }}
		</li>
		{{ range first 10 .Pages }}<ul class="post__summary">
			<h4><a href="{{ .RelPermalink }}">{{ .Title }}</a></h4>
			<div class="post__summary--content">{{ with .Params.description}}{{ . }}{{ else }}{{ .Summary }}{{ end }}</div>
			{{ if .Truncated }}<div class="post__summary--more"><a href="{{ .RelPermalink }}">Read More</a></div>
		{{ end }}</ul>
	{{ end }}</ul>{{ end }}
{{ partial "footer" . }}
```

These 15 lines of code get extrapolated out to, at time of writing, 65 lines of HTML. This number will adapt as more articles are posted and more information is displayed about said articles on the main page, but at no point in time will I have to edit the raw HTML of my landing page in order to add new articles to the listings shown there - and *that* is the power of HUGO. The "missing" 43 lines are imported from the various partials and content pieces. We can see that I immediately import the head partial, meaning with just that one line I've saved myself the trouble of copying 16 other lines manually. Additionally, any updates to the partial file will be applied to every instance wherein that partial is called, all automagically. After that I import the header, which itself imports another partial that controls the links in the top right of the site. With those two nested imports I'm already sitting at 25 lines of HTML with only a couple dozen keystrokes and two lines entered.

After those imports, I import the content of the respective markdown file; for the index.html page the respective markdown file is located at `/content/_index.md` and contains a bit of text that is unique to the landing page. Below that, we set up an unordered list using the `<ul>` tag, and then iterate over all of the sections we have, with sections in our case being subdirectories under in the `/contents/` directory. For each of these sections you can see we link it using a Rel(ative)Permalink (meaning a host-relative URL) and use the `.Title` of each section as our link text.

After that we check if the Section's respective `_index.md` file has a description set in the [front matter](https://www.gohugo.io/content-management/front-matter/), and if it does we print that out, if not we don't do anything.

Inside of each section, we then iterate over the first 10 pages (first meaning the 10 most recent based on their `date` [front matter](https://www.gohugo.io/content-management/front-matter/)), and print out the title and summary for each of them. If the summary section can't fit the entire article (common), it also adds a "Read More" link. After that I just end the previous if and for statements, close out the list tag, and display the footer.

## Conclusion

I'll leave it at that for now, but hopefully you can now see the power of HUGO for static site creation. With a few afternoons and going in with very little knowledge of writing HTML and especially of CSS syntax, I managed to create this entire website, which doesn't look to bad if you ask me! I thoroughly enjoyed using HUGO and genuinely think it is a worthwhile experience to undertake.
