---
title: "GPG - The  Ultimate Beginners Guide"
author: Jackson Lee
date: 2024-06-07
description: Setting up GPG does not have to be a hurculean task. I recently decided to generate a new GPG Key for use on this site, and decided it would be a good opportunity to make a guide walking through the seemingly complex world of GPG encryption.
categories: "projects"
tags: ["tech", "encryption", "guide"]
draft: false
---

# [GPG - It's *that* easy](/projects)

## Table of Contents

1. [GPG - A Quick Explaination](#gpg---a-quick-explainationtable-of-contents)
2. [Generating a Key Pair](#generating-a-key-pairtable-of-contents)
3. [Exporting your Key Pair](#exporting-your-key-pairtable-of-contents)
4. [Sharing your Key Pair](#sharing-your-key-pairtable-of-contents)
5. [Replacing your Key Pair](#replacing-your-key-pairtable-of-contents)

### [GPG - A Quick Explaination](#table-of-contents)
GPG (GNU Privacy Guard) is an open-source implementation of PGP (Pretty Good Privacy). GPG and PGP are commonly used for two things:

- Encryption: Preventing unwanted eyes from reading emails or opening files
- Signing: Digitally signing a document in a way that verifies it was actually you who signed it (think digital notary).

You likely (unknowingly) already use the latter when you receive software updates for your devices if you are using a Linux based system - many repositories use GPG signatures to verify the authenticity of the downloaded packages before installing them, in an effort to prevent installation of malware imposing as valid software updates. The former, however, is what we will primarily focus on today.

As outlined in [my article on encrypted email](/articles/encrypted-email-why-and-how), GPG encryption can be used to ensure that emails are securely encrypted before being sent. This does take some setting up, however, so let us get into the process and I will walk you through it as best I can.

#### What does that even mean?

I understand this can be overwhelming, so let me explain some important definitions:

The term "key" is used too many times to describe multiple things.

- Key: The term key commonly refers to a GPG key pair (henceforth referred to as a GPG key in this guide), which is a bundle of GPG keys composed of two parts:
	- Private Key - The first part of a GPG key, which is **NEVER** shared with anyone. It is used to digitally sign files and messages (think of it as equivalent to a notarized signature) as well as decrypt messages encrypted with the public key.
	- Public Key - The second part of a GPG key, which is shared with the world so that they may encrypt data such that only you may decrypt it through use of the private key.
- Passphrase: A GPG key is encrypted using this passphrase/password. When you use your private key to encrypt/decrypt a file or message, this passphrase is required in order to access the private key in order to perform the encryption/decryption.[^1]
- Keychain: Your keychain is where both your private and public keys are stored, as well as the public keys of your contacts. If you are trying to send an encrypted message to Alice, you must have her public key in your keychain in order to use it to encrypt the message.

> Key Takeaways (Pun Intended):
>
> Public Keys Encrypt
>
> Private Keys Decrypt

> __If you lose your Private Key and do not have a backup, you will not be able to decrypt messages and there is no way to recover it. You have been warned.__

### [Generating a Key Pair](#table-of-contents)

With those definitions out of the way, we can begin to actually generate our key. I will be walking through the commands to execute on Linux, but the overall process remains the same for any operating system. I will not insult your intelligence by telling you how to install GPG.


#### The Command Line Strikes Again
To generate a Key Pair without worrying about changing the defaults:

```sh
gpg --generate-key
```
If you would like to specify the encryption type and strength, you may instead run:

```sh
gpg --full-generate-key
```

In an attempt to keep this brief and uncomplicated, I will be using the former command in this guide. This will select only the default settings.

Once you run the command GPG will want to know what identity you would like to link to the newly generated key.

```sh
Real name: John Doe
Email address: email@example.com
Comment:
You selected this USER-ID:
    "John Doe <email@example.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?
```
Finally, it will ask you for a passphrase. Use a good, long, strong password that you do not use elsewhere, and never forget it. If you forget this password, you will not be able to unlock your private key.

#### Generated Key Parts

Once you enter the Passphrase, GPG will generate the Key and then tell you it has generated a `Revocation Certificate`. **Store this in a safe location**. It is generally recommended to print out a physical copy and store it in a secure location such as a fireproof safe or safety deposit box. If your key becomes compromised, this is your way to tell the world that they can no longer trust messages signed or encrypted with your key, and to not use that key to encrypt new messages.

```sh
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: revocation certificate stored as '/home/jackson/.local/share/gnupg/openpgp-revocs.d/F4F04AF2F9ED9B8D0029A911B8382D85210A12A5.rev'
public and secret key created and signed.

pub   ed25519 2024-06-08 [SC] [expires: 2027-06-08]
      F4F04AF2F9ED9B8D0029A911B8382D85210A12A5
uid                      John Doe <email@example.com>
sub   cv25519 2024-06-08 [E] [expires: 2027-06-08]
```
As you can see, my `Revocation Certificate` was stored at `/home/jackson/.local/share/gnupg/openpgp-revocs.d/F4F04AF2F9ED9B8D0029A911B8382D85210A12A5.rev`. The file name (`F4F0...12A5`) is the Unique Identifier for the Key, as can be seen in the output.

Now, at risk of complicating this further, you may notice the `[SC]` and `[E]` flags. Our Key Pair has actually generated a Key and a Sub Key. This is because there are multiple Keys inside of the Key Pair, one for Signing and Certifying, and one for Encrypting. This is what I was referring to earlier when I said "a _bundle_ of GPG Keys".

### [Exporting your Key Pair](#table-of-contents)

Every Key in your Key Chain will have a Unique Identifier associated with it. We can view this using the command:

```sh
gpg --list-keys
```

In our case, we can see that the Key ID from the output of our previous command is `F4F04AF2F9ED9B8D0029A911B8382D85210A12A5`. To export our key, we may use the following command:

#### Exporting the Private Key


We need to export our Private Key in order to back it up (3/2/1 backups, anyone?). To do this, we can run the following:

I prefer to include identifying information in the file name (the part that comes after the `>`), but really all you really need to include is the `.asc` file extension so you can easily see what file type it is, along with some reference to this being the public key.


```sh
gpg --armor --export-secret-keys F4F04AF2F9ED9B8D0029A911B8382D85210A12A5 > private-F4F04AF2F9ED9B8D0029A911B8382D85210A12A5-John-Doe-user@example.com.asc
```
>**Your private key should be handled with extreme care. Be deliberate when deciding to make a copy.**

#### Exporting the Public Key

```sh
gpg --armor --export F4F04AF2F9ED9B8D0029A911B8382D85210A12A5 > pubkey-F4F04AF2F9ED9B8D0029A911B8382D85210A12A5-John-Doe-user@example.com.asc
```

With this key exported, all that is left to do is to distribute it to the individuals you wish to use it to communicate with.

### [Sharing your Key Pair](#table-of-contents)

To ensure that our Public Key is easily accessible, we need to share it to a Key Server. Key Servers can be set up to automatically share keys between themselves, ensuring that if you upload to one, it will eventually propagate out to others. You can see which servers are currently active and well-connected [here](https://spider.pgpkeys.eu/graphs/). I will be using `sks.pgpkeys.eu` in my example to upload, search for, and download my own public key.

Uploading a Public Key to a server:
```sh
gpg --keyserver sks.pgpkeys.eu --send-keys 5F9FE7B6C39F1200D8CC389C5ED1134FD9974024
gpg: sending key 5ED1134FD9974024 to hkp://sks.pgpkeys.eu
```
Searching for a Public Key:
```sh
gpg --keyserver sks.pgpkeys.eu --search-keys jackson@jacksonlee.xyz
gpg: data source: http://sks.pgpkeys.eu:11371
(1)     Jackson Lee <jackson@jacksonlee.xyz>
        Jackson Lee <jacksonjlee@pm.me>
        Jackson Lee <jacksonjlee@proton.me>
        Jackson Lee <jacksonjlee@protonmail.com>
          4096 bit RSA key 5ED1134FD9974024, created: 2024-06-07
Keys 1-1 of 1 for "jackson@jacksonlee.xyz".  Enter number(s), N)ext, or Q)uit > 1
gpg: key 5ED1134FD9974024: "Jackson Lee <jackson@jacksonlee.xyz>" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1
```

Or, if you already know the key ID to use:

```sh
gpg --keyserver sks.pgpkeys.eu --recv-keys 5F9FE7B6C39F1200D8CC389C5ED1134FD9974024
gpg: key 5ED1134FD9974024: "Jackson Lee <jackson@jacksonlee.xyz>" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1
```
> It was processed as `unchanged` because I obviously already have my own public key

### [Replacing Your Key Pair](#table-of-contents)

Whether you're moving to a new computer, your data was corrupted, or you accidentally deleted your Private Key, disaster has struck and you need to restore from one of the secure backups you made previously. Worry not, the process is quite simple.

```sh
gpg --import private-F4F04AF2F9ED9B8D0029A911B8382D85210A12A5-John-Doe-user@example.com.asc
```
>**You will asked for the passphrase when importing your Private Key.**
>
>Remember when I said to ensure the backups were kept in a secure location __and__ to remember your passphrase?
>
>There is no help desk to call if you forget the passphrase or lose your private key, you are your own accountability.

Congratulations, you are done. The Private Key has been imported and you are free to go about your day. I tried to make this a simple to understand guide while still


[^1]: Some software, such as the Thunderbird email client, will seemingly encrypt and decrypt messages automatically without prompting you for your GPG password. This almost certainly means that your GPG Private Key is stored in a less manner than the alternative. Always assume that if you are not prompted to enter a password, there is a risk that your key is unsecured. In the case of Thunderbird, using a master password ensures that the database the Private Key is stored in will be encrypted. Make your own judgment call as to whether or not this is sufficiently secure for your use-case. Be mindful any time you are asked to provide your private key - in a perfect world you would never share it. Sharing it will always trade a bit of security for a bit of ease-of-use. Make the judgment call as necessary.
