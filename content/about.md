---
title: "About Me"
date: 2024-05-11
draft: false
---

# [About Me]({{<relref "/">}})

I am currently studying Cybersecurity and am very enthusiastic about FOSS software and projects. I have been using various Linux based operating systems as a daily driver since April of 2021 when I bought a [Pinebook Pro](https://www.pine64.com/product/14%e2%80%b3-pinebook-pro-linux-laptop-ansi-us-keyboard/) (which I highly recommend if you're interested in trying out Linux on 'bare metal') and became obsessed with tinkering with it. After about a month I installed [Arch](https://www.archlinux.org) onto my main machine and have never looked back. These days I'm running [Gentoo](https://www.gentoo.org) on my main machine with a full suite of [suckless software](https://www.suckless.org). The [Brave](https://www.brave.com) and [Firefox](https://www.mozilla.org/en-US/firefox/new) browsers are my recommended browsers, and while I am aware of the [Arkenfox Profile](https://www.github.com/arkenfox/user.js/) I do not currently use it as my daily driver and I instead opt to use [LibreWolf](https://librewolf.net). My desktop configuration and builds of the suckless utilities I currently and previously use, as well as the source code for this website can be found on my [gitlab](https://www.gitlab.com/jacksonjlee).

Apart from Gentoo, I use [Fedora](https://www.getfedora.org/) on my laptop, and an ARM variation of [Arch](https://www.archlinux.org/) on my Pinebook Pro. For anyone interested in trying Linux for the first time, I recommend [Linux Mint](https://www.linuxmint.com/), [Fedora](https://www.getfedora.com/), and [Debian](https://debian.org/). I have used all of them, have had great experiences, and cannot praise enough their stability and ease of use.


>If you would like to contact me, feel free send me an email at [jackson@jacksonlee.xyz](mailto:jackson@jacksonlee.xyz).
>My GPG Key for encrypted email can be found [here](/gpg/jacksonlee.asc) if you know how to use it.
>All legitimate emails from me will be signed with my GPG key.
>
>GPG Key Fingerprint: 5F9F E7B6 C39F 1200 D8CC 389C 5ED1 134F D997 4024
