# Personal Blog Source Code

This repository contains the source code for [my personal blog](https://jacksonlee.xyz). I have made it available for others to browse and use as they desire within the bounds of the license. This site was created using plain markdown files edited in [Neovim](https://neovim.io) and then generated into a static site using [Hugo](https://gohugo.io). I used a custom theme designed to fit my own tastes that can be found on [my GitLab](https://gitlab.com/jacksonjlee/simply-darkness).
